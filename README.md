# About
This repo contains the source I use to automatically generate [my curriculum vitae](http://vittorioerba.github.io/cv) as a HTML webpage and PDF from YAML and BibTeX input.

generate.py reads from cv.yaml and publications.bib and outputs LaTeX and HTML by using Jinja templates.

The project is forked from [this](https://github.com/bamos/cv) awsome project by Bamos. Thank you!

I manily converted the Markdown template in a HTML template, and adapted the cv.yaml format to my personal needs.
 
Below you can find part of the original README, to install and start using the utility.

# Building and running
The dependencies are included in `requirements.txt` and can be
installed
using `pip` with `pip3 install -r requirements.txt`.
On Mac or Linux, `make` will call generate.py and
build the LaTeX documents with `pdflatex` and `biber`.

The Makefile will also:

1. Stage to my website with `make stage`,
2. Start a local jekyll server of my website with updated
  documents with `make jekyll`, and
3. Push updated documents to my website with `make push`.

# Implementation details
## generate.py
1. Read `cv.yaml` into Python as a map and loop through the
   `order` vector,
   which maps a section key to the title to display.
   This is done so sections can be moved and hidden without
   deleting them.
2. Generate the LaTeX or Markdown content for every section by
   using the templates
   cv-section.tmpl.tex and
   cv-section.tmpl.md, which use
   [moderncv](http://www.ctan.org/pkg/moderncv).
   The conditional statements make the sections a little messy,
   but using a template for each section lets the order be changed
   solely by the `order` vector.
3. Generate the entire LaTeX or Markdown document by using
   the templates cv.tmpl.tex and
   cv.tmpl.md.

## Publications
All publications are stored as BibTeX in `publications.bib`.
The entries can be obtained from Google Scholar.
The order in the BibTeX file will be the order in
the output files.

BibTeX is built for integration with LaTeX, but producing
Markdown is not traditionally done from BibTeX files.
This repository uses BibtexParser to load the
bibliography into a map.
The data is manually formatted to mimic the LaTeX
IEEE bibliography style.


