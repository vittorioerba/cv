# Makefile to build PDF and html CV from YAML.

BLOG_DIR=$(HOME)/Git/vittorioerba.gitlab.io

all: gen/cv.pdf gen/cv.md

gen/cv.tex gen/cv.md: cv.yaml generate.py publications.bib \
	tmpl/cv-section.tmpl.tex tmpl/cv.tmpl.tex \
	tmpl/cv-section.tmpl.md tmpl/cv.tmpl.md
	./generate.py

gen/cv.pdf: gen/cv.tex publications.bib
	cd gen && \
	latexmk --pdf  && \
	latexmk -c

.PHONY: stage
stage: gen/cv.pdf gen/cv.html
	cp gen/cv.pdf $(BLOG_DIR)/data/Vittorio_Erba_cv.pdf
	cp gen/cv.html $(BLOG_DIR)

.PHONY: jekyll
jekyll: stage
	cd $(BLOG_DIR) && jekyll server

push: stage
	git -C $(BLOG_DIR) add $(BLOG_DIR)/data/Vittorio_Erba_cv.pdf
	git -C $(BLOG_DIR) add $(BLOG_DIR)/cv.html
	git -C $(BLOG_DIR) commit -m "Update vitae."
	git -C $(BLOG_DIR) push
	git add .
	git commit -m "Update vitae."
	git push origin master

.PHONY: clean
clean:
	rm -rf *.aux *.out *.log gen/* __pycache__
