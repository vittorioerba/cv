
# ~{{name}}~

~{ if name == "Education" }~
~{ for school in contents }~
~{{school.dates}}~ &#124; __<big>~{{school.school}}~</big>__ &#124; ~{{school.degree}}~
 
~{- if school.overallGPA }~
+ ~{{school.overallGPA}}~
~{- endif }~
~{- if school.thesis }~
+ Thesis: [~{{school.thesis}}~](~{{school.thesis_url}}~)
~{- endif }~
~{- if school.advisor }~
+ Advisor: ~{{school.advisor}}~
~{- endif }~
+ ~{{school.location}}~
~{ endfor }~

~{ elif name == "Teaching Experience" or name == "Talks" or  name.endswith("Experience") }~
~{ for n in contents }~

~{{n.dates}}~ &#124; __<big>~{{n.place}}~</big>__ &#124; ~{{n.title}}~
~{ if n.advisor -}~
  &#124; Advisor: ~{{n.advisor}}~
~{ endif -}~

+ ~{{n.location}}~

~{- if n.details -}~
~{- for detail in n.details }~
+ ~{{detail}}~ 

~{- endfor }~
~{- endif -}~
~{ endfor }~

~{ elif name == "Publications" }~
~{ for type in contents }~

__<big>~{{type['title']}}~.</big>__

~{{type['details']}}~

~{ endfor }~

~{ elif name == "Honors \\& Awards" or name == "Honors & Awards" or name == "Volounteer Experiences"}~
~{- for award in contents }~
  ~{- if 'url' in award }~
+ ~{{award['dates']}}~ &#124; [~{{award['title']}}~](~{{award['url']}}~) 
  ~{- else }~
+ ~{{award['dates']}}~ &#124; ~{{award['title']}}~ 
  ~{- endif }~
~{- if 'descr' in award }~
  + ~{{award['descr']}}~
~{- endif }~
~{- endfor }~

~{ elif name == "Projects" }~
~{ for k,v in contents.items() }~

<big>[~{{v['name']}}~](~{{v['url']}}~)</big>

~{ for detail in v['details'] }~
+ ~{{detail}}~
~{- endfor }~
~{ endfor }~

~{ elif name == "Activities" }~
~{- for activity in contents }~
+ ~{{activity}}~
~{- endfor }~

~{ elif name == "Skills" }~
~{- for skill in contents }~
+ __~{{skill['title']}}~.__ 
~{- for det in skill['details'] }~
+ ~{{ det }}~
~{- endfor }~
~{- endfor }~

~{ else }~
~{{contents}}~
~{ endif }~
