---
layout: default
title: CV
---

<!-- <style>
h1 {
  color:#008cba;
}
</style> -->

# ~{{name.first}}~ ~{{name.last}}~: Curriculum vitæ

[PDF](~{{pdf}}~) &#124;
[source](~{{src}}~) &#124;
[email](mailto:~{{email}}~) &#124;
Generated ~{{today}}~.

~{{body}}~
